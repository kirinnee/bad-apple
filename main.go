package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"time"
)

func read( s string) string {
	b, _ := ioutil.ReadFile(s)
	return string(b)
}

func main() {
	var arr [6571]string
	for index := range arr {
		arr[index] = read("./out/BA" + strconv.Itoa(index) + ".txt")
	}
	i := 0
	for range time.Tick(time.Millisecond * 30) {
		if i > 6570{
			break
		}
		//fmt.Println("\033[H\033[2J")
		fmt.Println(arr[i])
		i++
	}

}